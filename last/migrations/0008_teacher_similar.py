# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last', '0007_auto_20170612_0154'),
    ]

    operations = [
        migrations.AddField(
            model_name='teacher',
            name='similar',
            field=models.ManyToManyField(blank=True, null=True, related_name='similar_rel_+', to='last.Teacher'),
        ),
    ]
