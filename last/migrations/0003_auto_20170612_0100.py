# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('last', '0002_auto_20170610_0106'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 12, 1, 0, 7, 840991)),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 12, 1, 0, 7, 841666)),
        ),
    ]
