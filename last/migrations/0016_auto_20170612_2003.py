# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last', '0015_student_email_address'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=200, blank=True, null=True)),
                ('students', models.ManyToManyField(to='last.Student')),
            ],
        ),
        migrations.RenameField(
            model_name='teacher',
            old_name='students',
            new_name='groups',
        ),
    ]
