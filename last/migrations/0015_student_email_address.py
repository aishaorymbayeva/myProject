# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last', '0014_student_parent_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='email_address',
            field=models.EmailField(max_length=254, blank=True, null=True),
        ),
    ]
