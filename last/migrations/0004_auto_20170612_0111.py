# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last', '0003_auto_20170612_0100'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student',
            name='date',
        ),
        migrations.RemoveField(
            model_name='teacher',
            name='date',
        ),
        migrations.AlterField(
            model_name='teacher',
            name='students',
            field=models.ManyToManyField(null=True, to='last.Student'),
        ),
    ]
