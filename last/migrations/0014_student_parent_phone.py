# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last', '0013_student_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='parent_phone',
            field=models.IntegerField(max_length=200, blank=True, null=True),
        ),
    ]
