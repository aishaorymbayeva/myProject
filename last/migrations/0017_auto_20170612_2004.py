# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last', '0016_auto_20170612_2003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='groups',
            field=models.ManyToManyField(to='last.Group'),
        ),
    ]
