# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=100, unique=True)),
                ('surname', models.CharField(max_length=100, unique=True)),
                ('description', models.TextField(max_length=800, blank=True, null=True)),
                ('date', models.DateTimeField(default=datetime.datetime(2017, 6, 10, 0, 58, 47, 191103))),
            ],
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=200, unique=True)),
                ('description', models.TextField(max_length=300, blank=True, null=True)),
                ('date', models.DateTimeField(default=datetime.datetime(2017, 6, 10, 0, 58, 47, 191706))),
                ('similar', models.ManyToManyField(blank=True, null=True, related_name='similar_rel_+', to='last.Teacher')),
                ('students', models.ManyToManyField(to='last.Student')),
            ],
        ),
    ]
