# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('last', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 10, 1, 6, 30, 999780)),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 6, 10, 1, 6, 31, 379)),
        ),
    ]
