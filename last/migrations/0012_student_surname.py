# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('last', '0011_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='surname',
            field=models.CharField(max_length=200, blank=True, null=True),
        ),
    ]
