from django.db import models
from django.contrib.auth.models import User
import datetime



class Student(models.Model):
    user = models.OneToOneField(User,null=True)
    name = models.CharField(max_length=200)
    surname = models.CharField(max_length=200,blank=True,null=True)
    email_address = models.EmailField(null=True,blank=True)
    phone = models.IntegerField(max_length=200,null=True,blank=True)
    parent_phone = models.IntegerField(max_length=200, null=True, blank=True)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Group(models.Model):
    students = models.ManyToManyField(Student)
    name = models.CharField(max_length=200, null=True,blank=True)

    def __str__(self):
        return self.name


class Teacher(models.Model):
    user = models.OneToOneField(User, null=True)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    groups = models.ManyToManyField(Group)
    similar = models.ManyToManyField("self",null  =True,blank=True)

    def __str__(self):
        return self.name

class Owner(models.Model):
    user = models.OneToOneField(User, null=True)
    name = models.CharField(max_length=200)
    teachers = models.ManyToManyField(Teacher)

    def __str__(self):
        return self.name





