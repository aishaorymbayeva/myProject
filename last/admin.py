from django.contrib import admin
from last.models import Student, Teacher, Owner, Group


class StudentAdmin(admin.ModelAdmin):
    list_display = ('user',)
    search_fields = ['name']

class GroupAdmin(admin.ModelAdmin):
    search_fields = ['name']
    filter_horizontal = ('students',)


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('user',)
    search_fields = ['name']
    filter_horizontal = ('similar','groups',)

class OwnerAdmin(admin.ModelAdmin):
    list_display = ('user',)
    search_fields = ['name']
    filter_horizontal = ('teachers',)


admin.site.register(Student,StudentAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Teacher,TeacherAdmin)
admin.site.register(Owner,OwnerAdmin )


